import os
from random import shuffle

import cv2
import numpy as np
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
from tqdm import tqdm

GLOBAL_DIR = 'datasetBird/global'
TRAIN_DIR = 'datasetBird/test_set'
TEST_DIR = 'datasetBird/train_set'
IMG_SCALE = 1
IMG_SIZE = 200
N_EPOCH = 100
N_CHANNEL = 3
LR = 1e-3
TEST_NUMBER = 'Quinto'
MODEL_NAME = 'Test/' + TEST_NUMBER + ' Test/bird'
TRAIN_DATA_LOAD = 'Test/' + TEST_NUMBER + ' Test/train_data_bird.npy'
TEST_DATA_LOAD = 'Test/' + TEST_NUMBER + ' Test/test_data_bird.npy'
load_model = True


def label_img(img, dictionary_class_to_int):
    list_name = [0] * len(dictionary_class_to_int)
    word_label = (img.split('.')[0]).replace("_", "").lower()
    result = ''.join([i for i in word_label if not i.isdigit()])
    x = dictionary_class_to_int.get(result)
    list_name[x] = 1
    return list_name


def create_train_data(dictionary_class_to_int):
    training_data = []
    for img in tqdm(os.listdir(TRAIN_DIR)):
        label = label_img(img, dictionary_class_to_int)
        path = os.path.join(TRAIN_DIR, img)
        img = cv2.imread(path, IMG_SCALE)
        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
        training_data.append([np.array(img), np.array(label)])
    shuffle(training_data)
    np.save(TRAIN_DATA_LOAD, training_data)
    return training_data


def process_test_data():
    testing_data = []
    for img in tqdm(os.listdir(TEST_DIR)):
        path = os.path.join(TEST_DIR, img)
        img_num = img.split('.')[0]
        img = cv2.imread(path, IMG_SCALE)
        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
        testing_data.append([np.array(img), img_num])
    np.save(TEST_DATA_LOAD, testing_data)
    return testing_data


def create_model():
    convnet = input_data(shape=[None, IMG_SIZE, IMG_SIZE, N_CHANNEL], name='input')

    convnet = conv_2d(convnet, 32, 2, activation='relu', name="l1")
    convnet = max_pool_2d(convnet, 2, name="l2")

    convnet = conv_2d(convnet, 64, 2, activation='relu', name="l3")
    convnet = max_pool_2d(convnet, 2, name="l4")

    convnet = conv_2d(convnet, 32, 2, activation='relu', name="l5")
    convnet = max_pool_2d(convnet, 2, name="l6")

    convnet = conv_2d(convnet, 64, 2, activation='relu', name="l7")
    convnet = max_pool_2d(convnet, 2, name="l8")

    convnet = conv_2d(convnet, 32, 2, activation='relu', name="l9")
    convnet = max_pool_2d(convnet, 2, name="l10")

    convnet = conv_2d(convnet, 64, 2, activation='relu', name="l11")
    convnet = max_pool_2d(convnet, 2, name="l12")

    convnet = fully_connected(convnet, 1024, activation='relu', name="l13")
    convnet = dropout(convnet, 0.8)

    convnet = fully_connected(convnet, 200, activation='softmax', name="l14")
    convnet = regression(convnet, optimizer='adam', learning_rate=LR, loss='categorical_crossentropy', name='targets')

    model = tflearn.DNN(convnet, tensorboard_dir='log')
    return model


def train_first_model(model, train_data):
    train = train_data
    test = train_data

    x = np.array([i[0] for i in train]).reshape(-1, IMG_SIZE, IMG_SIZE, N_CHANNEL)
    y = [i[1] for i in train]

    test_x = np.array([i[0] for i in test]).reshape(-1, IMG_SIZE, IMG_SIZE, N_CHANNEL)
    test_y = [i[1] for i in test]

    model.fit({'input': x}, {'targets': y}, n_epoch=N_EPOCH, validation_set=({'input': test_x}, {'targets': test_y}),snapshot_step=1000, show_metric=True, run_id=MODEL_NAME)
    model.save(MODEL_NAME)


def test_model(model, test_data, dictionary_int_to_class):
    accurancy = 0
    for num, data in enumerate(test_data):
        img_data = data[0]
        img_class = test_data[num][1]
        img_class_label = (img_class.split('.')[0]).replace("_", "").lower()
        img_class_result = ''.join([i for i in img_class_label if not i.isdigit()])
        data = img_data.reshape(IMG_SIZE, IMG_SIZE, N_CHANNEL)
        model_out = model.predict([data])
        max_index = np.argmax(model_out)
        str_label_out = dictionary_int_to_class.get(max_index)
        if str_label_out == img_class_result:
            accurancy += 1
    accurancy = accurancy / len(test_data)
    print(accurancy)

def make_dictionaries(list_classes):
    i = 0
    dictionary_class_to_int = {}
    dictionary_int_to_class = {}
    for element in list_classes:
        name = ((element.split(".")[1]).replace("_", "")).lower()
        dictionary_class_to_int[name] = i
        dictionary_int_to_class[i] = name
        i += 1
    return dictionary_int_to_class, dictionary_class_to_int


def main():
    dictionary_int_to_class, dictionary_class_to_int = make_dictionaries(os.listdir(GLOBAL_DIR))
    if os.path.exists(TRAIN_DATA_LOAD):
        train_data = np.load(TRAIN_DATA_LOAD)
        print('Train Data Loaded!!')
    else:
        train_data = create_train_data(dictionary_class_to_int)
    if os.path.exists(TEST_DATA_LOAD):
        test_data = np.load(TEST_DATA_LOAD)
        print('Test Data Loaded!!')
    else:
        test_data = process_test_data()

    if load_model:
        # training new model
        model = create_model()
        train_first_model(model, train_data)
        test_model(model, test_data, dictionary_int_to_class)
    else:
        # test model
        model = create_model()
        model.load(MODEL_NAME, weights_only=True)
        test_model(model, test_data, dictionary_int_to_class)


if __name__ == "__main__":
    main()
